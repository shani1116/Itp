/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

//import HR.AddEmployee;
import HR.AddCase;
import HR.AddEmployee;
import HR.AddEntry;
import HR.AddLeave;
import HR.App_Ign_Leave;
import HR.DeleteLeave;
import HR.SearchEmployee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Shanika
 */
public class HRmanager extends Employee{
    
    //Creates the database connection
    Connection conn= dbConnect.connect();;
    PreparedStatement pst=null;
    ResultSet rs=null;
    
    //Creates an Employee object
    Employee e = new Employee();
    
    //Creates a case object
    discase c1 = new discase();
    
    //Creates a leave object
    leave l = new leave();
    
    //Creates an attendance object
    Attendance a = new Attendance();
    
    public void addemp(String eid, String fn, String ln, String desg, String n, String dept, String add, String ph){
        e.Empid = eid;
        e.fname = fn;
        e.lname = ln;
        e.designation = desg;
        e.NIC = n;
        e.dep = dept;
        e.address = add;
        e.phone = ph;
        
        String sq = "INSERT INTO employee VALUES ('"+ e.Empid +"', '"+ e.fname +"', '"+ e.lname +"', '"+ e.designation +"', '"+ e.NIC +"', '"+ e.dep +"', '"+ e.address +"', '"+ e.phone +"')";
        
        try {
            pst = conn.prepareStatement(sq);
            pst.execute();
            JOptionPane.showMessageDialog(null,"One employee added to the database!");
            
        } catch (SQLException ex) {
            System.out.println("Error when adding to database! Please check connectivity.");
            Logger.getLogger(AddEmployee.class.getName()).log(Level.SEVERE, null, ex);
            
        }
    }
    
    public void delemp(String eid){
        e.Empid = eid;
        
        String sq = "DELETE FROM employee WHERE emp_id = '"+ e.Empid +"' ";
        
        try {
            pst = conn.prepareStatement(sq);
            pst.execute();
             JOptionPane.showMessageDialog(null,"One employee deleted from the database!");
            
        } catch (SQLException ex) {
           System.out.println("Error when accessing database! Please check connectivity.");
           
        }
    }
    
    public void updateemp(String eid, String fn, String ln, String desg, String n, String dept, String add, String ph){
        e.Empid = eid;
        e.fname = fn;
        e.lname = ln;
        e.designation = desg;
        e.NIC = n;
        e.dep = dept;
        e.address = add;
        e.phone = ph;
        
        String sq = "UPDATE employee SET emp_id = '"+ e.Empid +"', fname = '"+ e.fname +"', lname = '"+ e.lname +"', designation = '"+ e.designation +"', NIC = '"+ e.NIC +"', department = '"+ e.dep +"', address = '"+ e.address +"', phone_no = '"+ e.phone +"' WHERE emp_id = '"+ e.Empid +"' ";
        
        try {
            pst = conn.prepareStatement(sq);
            pst.execute();
            JOptionPane.showMessageDialog(null,"One employee was changed in the databse!");
            
        } catch (SQLException ex) {
            System.out.println("Error when adding to database! Please check connectivity.");
            
        }
    }
    
    public void searchemp(String some){
        
        if(some.equals("EmpID"))
            e.Empid = some;
        else if(some.equals("Fname"))
            e.fname=some;
         else if(some.equals("Lname"))
            e.lname=some;
         else if(some.equals("NIC"))
            e.NIC=some;
         else
            e.designation=some;
    
        String sql1 = "SELECT emp_id, fname, lname, designation, NIC, department, address, phone_no FROM employee WHERE emp_id='"+ e.Empid +"'";
        
        try {
            pst = conn.prepareStatement(sql1);
            rs=pst.executeQuery();
            //JOptionPane.showMessageDialog(null,"One employee was changed in the databse!");
            //SearchEmployee.jTable1.setModel(DbUtils.resultSetToTableModel(rs));
            
        } catch (SQLException ex) {
            System.out.println("Error when adding to database! Please check connectivity.");
        }
        
    }
    
    public void addleave(String eid, String sd, String ed, String lt, String wp, String r, int d){
        
        l.empID = eid;
        l.sdate = sd;
        l.edate = ed;
        l.type = lt;
        l.workplace = wp;
        l.reason = r;
        l.duration = d;
    
        
        String q="INSERT INTO `leave`(`start_date`, `end_date`, `type`, `workplace`, `reason`, `duration`, `emp_id`) VALUES ('"+ l.sdate +"','"+ l.edate +"','"+ l.type +"','"+ l.workplace +"','"+ l.reason +"','"+ l.duration +"','"+ l.empID +"')";
        
        try {
            pst = conn.prepareStatement(q);
            pst.execute();
            JOptionPane.showMessageDialog(null,"One leave record added to the databse!");
        } catch (SQLException ex) {
            
            Logger.getLogger(AddLeave.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void delleave(int lid, String eid){
        
        l.leaveID = lid;
        l.empID = eid;
        
        String q="DELETE FROM leave WHERE leave_id="+l.leaveID;
        
        try {
            pst = conn.prepareStatement(q);
            pst.execute();
            JOptionPane.showMessageDialog(null,"One leave record deleted from the databse!");
        } catch (SQLException ex) {
            
            Logger.getLogger(DeleteLeave.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void searchleave(){
    
    }
    
    public void addcase(String eid, String cn, String n, String des, String cb, String co, String dept, String stat){
        
        c1.empID = eid;
        c1.casename = cn;
        c1.name = n;
        c1.desc = des;
        c1.createdby = cb;
        c1.createdon = co;
        c1.dep = dept;
        c1.status = stat;
        
        System.out.println(c1.dep);
        System.out.println(c1.status);
        //String q = "INSERT INTO case (case_id,case_name,emp_id,emp_name,description,createdby,createdon,dep,status) VALUES (0,'"+ c1.casename +"', '"+ c1.empID +"', '"+ c1.name +"', '"+ c1.desc +"', '"+ c1.createdby +"', '"+ c1.createdon +"','"+ c1.dep +"', '"+ c1.status +"')";
        String q = "INSERT INTO `case`(`case_name`, `emp_id`, `emp_name`, `description`, `createdby`, `createdon`, `dep`, `status`) VALUES (['"+ c1.casename +"', '"+ c1.empID +"', '"+ c1.name +"', '"+ c1.desc +"', '"+ c1.createdby +"', '"+ c1.createdon +"','"+ c1.dep +"', '"+ c1.status +"'";
        
            try{
            pst = conn.prepareStatement(q);
            pst.execute();
            JOptionPane.showMessageDialog(null,"One case was changed in the databse!");
            }
            catch(SQLException e){
            Logger.getLogger(AddCase.class.getName()).log(Level.SEVERE, null, e);
            System.out.println("Error when adding to database! Please check connectivity.");
            }
    }
    
    public void addent(String eid, String n, String d, String st){
        
        a.empID = eid;
        a.name = n;
        a.date = d;
        a.status = st;
        
        String q = "INSERT INTO attendance VALUES ('"+ a.name +"', '"+ a.date +"', '"+ a.status +"','"+ a.empID +"')";
        
        try{
            pst = conn.prepareStatement(q);
            pst.execute();
            JOptionPane.showMessageDialog(null,"One entry added to the database");
            }
            catch(SQLException e){
            Logger.getLogger(AddEntry.class.getName()).log(Level.SEVERE, null, e);
            System.out.println("Error when adding to database! Please check connectivity.");
            }
    }
    
    public void approveleave(String eid, int lid, String a){
    
        l.empID=eid;
        l.leaveID=lid;
        l.appr=a;
        
        System.out.println(l.empID);
         System.out.println(l.appr);
        
        if(l.appr.equals("Yes")){
            
            try{
            String sql = "UPDATE `leave` SET `approvement`='yes' WHERE `emp_id`='"+ l.empID +"'";
            
           
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null,"One leave approved!");
            }
            catch(SQLException e){
            Logger.getLogger(App_Ign_Leave.class.getName()).log(Level.SEVERE, null, e);
            //System.out.println("Error when adding to database! Please check connectivity.");
            }
        }
        else{
            
            String sql = "UPDATE `leave` SET `approvement`='no' WHERE `emp_id`='"+ l.empID +"'";
            
            try{
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null,"One leave Not approved!");
            }
            catch(SQLException e){
            Logger.getLogger(AddLeave.class.getName()).log(Level.SEVERE, null, e);
            System.out.println("Error when adding to database! Please check connectivity.");
            }
        }
        
    }
    
}
