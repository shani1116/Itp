/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Shanika
 */
public class User {
    
    
    //Creates the database connection
    Connection conn= dbConnect.connect();;
    PreparedStatement pst=null;
    ResultSet rs = null;
    
    
    public String username;
    public String Password;
    public int priv_no;
    public String empId;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return Password;
    }

    public int getPriv_no() {
        return priv_no;
    }
    
    public int getPriv_no(String us){
        
        String p = "SELECT username FROM user WHERE username="+us;
        
       /* if(p.matches("Admin"))
            return 1;
        else if(p.matches("HRM"))
            return 2;
        else if(p.matches("FinM"))
            return 3;
        else if(p.matches("MaintM"))
            return 4;
        else if(p.matches("CusM"))
            return 5;
        else if(p.matches("TransM"))
            return 6;
        else if(p.matches("EventM"))
            return 7;
        else if(p.matches("EntM"))
            return 8;
        else if(p.matches("CarM"))
            return 9;
        else if(p.matches("GameC"))
            return 10;
        else if(p.matches("CarC"))
            return 11;
        else if(p.matches("Cus"))
            return 12;
        else
            return 0;*/
        
        try{
            
            pst=conn.prepareStatement(p);
            rs=pst.executeQuery();
            
        }
        catch(Exception e){
        
        }
    
        
        switch(p){
            case "Admin":
                return 1;
                //break;
            case "HRM":
                return 2;
                //break;
            case "FinM":
                return 3;
            case "MaintM":
                return 4;
            case "CusM":
                return 5;
            case "TransM":
                return 6;
            case "EventM":
                return 7;
            case "EntM":
                return 8;
            case "CarM":
                return 9;
            case "GameC":
                return 10;
            case "CarC":
                return 11;
            default:
                return 0;
                
        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

   
    
    public void login(String u, String p){
    
            this.username = u;
            this.Password = p;
            
            int priv = this.getPriv_no(u);
                    
            String sq = "SELECT username,password FROM user WHERE username=? AND password=?";
    
            try {
            pst = conn.prepareStatement(sq);
            pst.setString(1,this.username);
            pst.setString(2,this.Password);
            rs=pst.executeQuery();
            
            if(rs.next()){
             switch(priv){
                 case 1:
                    JOptionPane.showMessageDialog(null,"Logged in as Admin!");
                    break;
                 case 2:
                    JOptionPane.showMessageDialog(null,"Logged in as HR Manager!");
                    break;
                 case 3:
                    JOptionPane.showMessageDialog(null,"Logged in as Finance Manager!");
                    break;
                 case 4:
                    JOptionPane.showMessageDialog(null,"Logged in as Maintainance Manager!");
                    break;
                 case 5:
                    JOptionPane.showMessageDialog(null,"Logged in as Customer Manager!");
                    break;
                 case 6:
                    JOptionPane.showMessageDialog(null,"Logged in as Trans/Inventory Manager!");
                    break;
                 case 7:
                    JOptionPane.showMessageDialog(null,"Logged in as Event Manager!");
                    break;
                 case 8:
                    JOptionPane.showMessageDialog(null,"Logged in as GameZone Manager!");
                    break;
                 case 9:
                    JOptionPane.showMessageDialog(null,"Logged in as Car Park Manager!");
                    break;
                 case 10:
                    JOptionPane.showMessageDialog(null,"Logged in as Game Counter!");
                    break;
                 case 11:
                    JOptionPane.showMessageDialog(null,"Logged in as Car Park Counter!");
                    break;
                 default:
                    JOptionPane.showMessageDialog(null,"Invalid username or password");
             }
                 
             }
            }
            
    
        catch (SQLException ex) {
            System.out.println("Error when connecting to database! Please check connectivity.");
            
        }
    
    }
    
    public void logout(){
        
        
    } 
    
    //public boolean AuthUsers(String user, String pass){
        
        /*Connection conn = null;
        PreparedStatement pst = null;
        
        conn = dbConnect.connect();
        
        String q = "SELECT (username, password) FROM user WHERE usename=user AND passowrd=pass";
        
        //if
        */
    //}
}
