/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Event;

import Event.Classes.Event;
import Event.Classes.ExternalAffairsManager;
import java.text.SimpleDateFormat;

/**
 *
 * @author Navodya
 */
public class Location extends javax.swing.JInternalFrame {

    /**
     * Creates new form Location
     */
    Event e=new Event();
    ExternalAffairsManager u=new ExternalAffairsManager();
    public Location() {
        initComponents();
    }

    String sd;
    String ed;
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        BanHallButt = new javax.swing.JButton();
        cockButt = new javax.swing.JButton();
        outDButt = new javax.swing.JButton();
        ExhibiHButt = new javax.swing.JButton();
        openHallButt = new javax.swing.JButton();
        FunHallButt = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        locAvali = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Display = new javax.swing.JTextArea();
        SearchStart = new com.toedter.calendar.JDateChooser();
        SearchEnd = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();

        setClosable(true);
        setForeground(java.awt.Color.white);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Locations");

        jLabel3.setFont(new java.awt.Font("Georgia", 0, 11)); // NOI18N
        jLabel3.setText("Search  Start date");

        jPanel1.setBackground(new java.awt.Color(0, 102, 153));
        jPanel1.setForeground(new java.awt.Color(0, 102, 153));

        jLabel6.setBackground(new java.awt.Color(0, 102, 153));
        jLabel6.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Locations");

        jLabel7.setBackground(new java.awt.Color(0, 102, 153));
        jLabel7.setFont(new java.awt.Font("Georgia", 0, 11)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText(" (Rs per day)");

        BanHallButt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/icons8_Flower_26px.png"))); // NOI18N
        BanHallButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BanHallButtActionPerformed(evt);
            }
        });

        cockButt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/icons8_Cocktail_26px.png"))); // NOI18N
        cockButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cockButtActionPerformed(evt);
            }
        });

        outDButt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/icons8_Oak_Tree_26px.png"))); // NOI18N
        outDButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outDButtActionPerformed(evt);
            }
        });

        ExhibiHButt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/icons8_Gallery_26px.png"))); // NOI18N
        ExhibiHButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExhibiHButtActionPerformed(evt);
            }
        });

        openHallButt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/icons8_Map_Marker_26px.png"))); // NOI18N
        openHallButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openHallButtActionPerformed(evt);
            }
        });

        FunHallButt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/icons8_Red_Carpet_26px.png"))); // NOI18N
        FunHallButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FunHallButtActionPerformed(evt);
            }
        });

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Banquet Hall");

        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Cocktail Lounge");

        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("OutDoor");

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Exhibiton Hall");

        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Open Area");

        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Function Hall");

        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("2Laks");

        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("50k-75k");

        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("25k");

        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("1.5laks");

        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("75k");

        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("1Lak");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jLabel7))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(23, 23, 23))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(10, 10, 10)
                            .addComponent(jLabel8))
                        .addComponent(BanHallButt, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(cockButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel15)))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(outDButt, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel16))
                            .addComponent(jLabel10))
                        .addGap(56, 56, 56)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(ExhibiHButt, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addComponent(openHallButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(53, 53, 53)
                                .addComponent(jLabel12)))
                        .addGap(45, 45, 45)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(FunHallButt, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel13)))
                        .addContainerGap(105, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel18)
                        .addGap(109, 109, 109)
                        .addComponent(jLabel19)
                        .addGap(127, 127, 127))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(outDButt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                        .addComponent(ExhibiHButt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(openHallButt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(FunHallButt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cockButt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BanHallButt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel18)
                        .addComponent(jLabel17)
                        .addComponent(jLabel16)
                        .addComponent(jLabel15)
                        .addComponent(jLabel19))
                    .addComponent(jLabel14))
                .addGap(0, 33, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Georgia", 0, 11)); // NOI18N
        jLabel1.setText("End date");

        locAvali.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(null, new java.awt.Color(153, 153, 153)), "Availabitiy", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Georgia", 0, 14), new java.awt.Color(0, 102, 153))); // NOI18N

        Display.setBackground(new java.awt.Color(204, 204, 204));
        Display.setColumns(20);
        Display.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        Display.setRows(5);
        jScrollPane1.setViewportView(Display);

        javax.swing.GroupLayout locAvaliLayout = new javax.swing.GroupLayout(locAvali);
        locAvali.setLayout(locAvaliLayout);
        locAvaliLayout.setHorizontalGroup(
            locAvaliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(locAvaliLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );
        locAvaliLayout.setVerticalGroup(
            locAvaliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(locAvaliLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jButton1.setFont(new java.awt.Font("Georgia", 0, 11)); // NOI18N
        jButton1.setText("Clear");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(locAvali, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(125, 125, 125)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(SearchEnd, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
                        .addComponent(SearchStart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(locAvali, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(35, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(SearchStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(SearchEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(38, 38, 38)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(105, 105, 105))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ExhibiHButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExhibiHButtActionPerformed
          SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            sd = dateFormat.format(SearchStart.getDate());
            
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd"); 
           ed =dateFormat2.format(SearchEnd.getDate());
           
           String val1= "SELECT startDate FROM event WHERE locId LIKE 'EH%' ";
           String val2= "SELECT endDate FROM event WHERE locId LIKE 'EH%' ";
           
             if(val1.equals(sd)==true||val2.equals(ed)==true)
               Display.setText("\n \n  UNAVAILABLE \n  the place is already reserved");
           else
                Display.setText("\n \n  AVAILABLE  \n  Can add an event ");
           
           
    }//GEN-LAST:event_ExhibiHButtActionPerformed

    private void openHallButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openHallButtActionPerformed
       SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            sd = dateFormat.format(SearchStart.getDate());
            
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd"); 
           ed =dateFormat2.format(SearchEnd.getDate());
           
           String val1= "SELECT startDate FROM event WHERE locId LIKE 'OH%' ";
           String val2= "SELECT endDate FROM event WHERE locId LIKE 'OH%' ";
           
            if(val1.equals(sd)==true||val2.equals(ed)==true)
               Display.setText("\n \n  UNAVAILABLE \n  the place is already reserved");
           else
                Display.setText("\n \n  AVAILABLE  \n  Can add an event ");
           
    }//GEN-LAST:event_openHallButtActionPerformed

    private void cockButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cockButtActionPerformed
         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            sd = dateFormat.format(SearchStart.getDate());
            
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd"); 
           ed =dateFormat2.format(SearchEnd.getDate());
           
           String val1= "SELECT startDate FROM event WHERE locId LIKE 'CL%' ";
           String val2= "SELECT endDate FROM event WHERE locId LIKE 'CL%' ";
           
        if(val1.equals(sd)==true||val2.equals(ed)==true)
               Display.setText("\n \n  UNAVAILABLE \n  the place is already reserved");
           else
                Display.setText("\n \n  AVAILABLE  \n  Can add an event ");
           
    }//GEN-LAST:event_cockButtActionPerformed

    private void outDButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outDButtActionPerformed
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            sd = dateFormat.format(SearchStart.getDate());
            
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd"); 
           ed =dateFormat2.format(SearchEnd.getDate());
           
           String val1= "SELECT startDate FROM event WHERE locId LIKE 'OD%' ";
           String val2= "SELECT endDate FROM event WHERE locId LIKE 'OD%' ";
           
           if(val1.equals(sd)==true||val2.equals(ed)==true)
               Display.setText("\n \n  UNAVAILABLE \n  the place is already reserved");
           else
                Display.setText("\n \n  AVAILABLE  \n  Can add an event ");
           
    }//GEN-LAST:event_outDButtActionPerformed

    private void BanHallButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BanHallButtActionPerformed
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            sd = dateFormat.format(SearchStart.getDate());
            
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd"); 
           ed =dateFormat2.format(SearchEnd.getDate());
           
           String val1= "SELECT startDate FROM event WHERE locId LIKE 'BH%' ";
           String val2= "SELECT endDate FROM event WHERE locId LIKE 'BH%' ";
           
        if(val1.equals(sd)==true||val2.equals(ed)==true)
               Display.setText("\n \n  UNAVAILABLE \n  the place is already reserved");
           else
                Display.setText("\n \n  AVAILABLE  \n  Can add an event ");
           
    }//GEN-LAST:event_BanHallButtActionPerformed

    private void FunHallButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FunHallButtActionPerformed
         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            sd = dateFormat.format(SearchStart.getDate());
            
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd"); 
           ed =dateFormat2.format(SearchEnd.getDate());
           
           String val1= "SELECT startDate FROM event WHERE locId LIKE 'FH%' ";
           String val2= "SELECT endDate FROM event WHERE locId LIKE 'FH%' ";
           
          if(val1.equals(sd)==true||val2.equals(ed)==true)
               Display.setText("\n \n  UNAVAILABLE \n the place is already reserved");
           else
                Display.setText("\n \n  AVAILABLE  \n Can add an event ");
           
    }//GEN-LAST:event_FunHallButtActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      
        SearchStart.setDate(null);
        SearchEnd.setDate(null);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BanHallButt;
    private javax.swing.JTextArea Display;
    private javax.swing.JButton ExhibiHButt;
    private javax.swing.JButton FunHallButt;
    private com.toedter.calendar.JDateChooser SearchEnd;
    private com.toedter.calendar.JDateChooser SearchStart;
    private javax.swing.JButton cockButt;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel locAvali;
    private javax.swing.JButton openHallButt;
    private javax.swing.JButton outDButt;
    // End of variables declaration//GEN-END:variables
}
